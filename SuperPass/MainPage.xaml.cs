﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace SuperPass
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }




	    private void ButtonProceed_OnClick(object sender, RoutedEventArgs e)
	    {
		    var oldCode = txtOld.Text.Trim();
		    var newCode = DecodePass(oldCode);

		    txtNew.Text = newCode;

	    }


	    private string DecodePass(string pass)
	    {
			var lsSuperPass = "";
			double lsLog;
			int liInd;
			int liAsc;
			int liTmp;
			double llOffset;

			for (int i = 0; i < pass.Length; i++)
			{

				liAsc = pass[i] - 47;
				//DecimalFormat f = new DecimalFormat("##.00000000");
				//String formattedValue = f.format(Math.log(liAsc));
				var formattedValue = Math.Log(liAsc).ToString("##.00000000");
				//formattedValue = string.Format("{00:00000000}", Math.Log(liAsc));
				lsLog = Double.Parse(formattedValue);// Math.round(Math.log(liAsc), 8);
				
				var lsLogString = lsLog.ToString(NumberFormatInfo.InvariantInfo);
				if (lsLog <= 0 || lsLog.Equals(Double.NaN))
				{
					lsLogString = "0.00000000";
				}

				//liTmp = int.Parse(lsLogString.Substring(lsLogString.Length - 2, lsLogString.Length));
				liTmp = Convert.ToInt32(lsLogString.Substring(lsLogString.Length - 2));

				if (liTmp % 2 == 0)
				{
					//llOffset = Convert.ToInt32(lsLogString.Substring(3, 5)) % 26;
					llOffset = Convert.ToInt32(lsLogString.Substring(3, 2)) % 26;

					if (char.ToString((char)(65 + llOffset)).Equals("L"))
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(65 + llOffset + 1));
					}
					else if (char.ToString((char)(65 + llOffset)).Equals("O"))
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(65 + llOffset + 1));
					}
					else if (char.ToString((char)(65 + llOffset)).Equals("I"))
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(65 + llOffset + 1));
					}
					else
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(65 + llOffset));
					}
				}
				else if (liTmp % 3 == 0)
				{
					//llOffset = Convert.ToInt32(lsLogString.Substring(4, 6)) % 26;
					llOffset = Convert.ToInt32(lsLogString.Substring(4, 2)) % 26;

					if (char.ToString((char)(97 + llOffset)).Equals("l"))
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(97 + llOffset + 1));
					}
					else if (char.ToString((char)(97 + llOffset)).Equals("o"))
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(97 + llOffset + 1));
					}
					else
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(97 + llOffset));
					}
				}
				else
				{
					//llOffset = int.Parse(lsLogString.Substring(5, 7)) % 10;
					llOffset = int.Parse(lsLogString.Substring(5, 2)) % 10;

					if (char.ToString((char)(48 + llOffset)).Equals("0"))
					{
						lsSuperPass = lsSuperPass + "2";
					}
					else if (char.ToString((char)(48 + llOffset)).Equals("10"))
					{
						lsSuperPass = lsSuperPass + "9";
					}
					else
					{
						lsSuperPass = lsSuperPass + char.ToString((char)(48 + llOffset));
					}
				}


			}

			return lsSuperPass;
		}
	

    }
}
